package com.hendisantika.mailgunemailsender;

import com.hendisantika.mailgunemailsender.config.EmailSenderConfiguration;
import com.hendisantika.mailgunemailsender.entity.Email;
import com.hendisantika.mailgunemailsender.service.EmailSender;
import com.hendisantika.mailgunemailsender.util.EmailBuilder;
import com.hendisantika.mailgunemailsender.util.EmailValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest(classes = {
        EmailSender.class,
        EmailValidator.class,
        EmailBuilder.class
})
@EnableConfigurationProperties(EmailSenderConfiguration.class)
class MailgunEmailSenderApplicationTests {

    @Autowired
    EmailSender emailSender;

    @Autowired
    EmailBuilder emailBuilder;

    @Test
    public void testSendEmail() {
        Email email = emailBuilder.builder()
                .body(UUID.randomUUID().toString())
                .subject(UUID.randomUUID().toString())
                .useDefaultRecipients()
                .useDefaultSender()
                .build();
        //MailgunResponse response = emailSender.sendEmail(email);
    }

}

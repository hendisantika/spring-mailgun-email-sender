package com.hendisantika.mailgunemailsender.util;

import com.hendisantika.mailgunemailsender.config.EmailSenderConfiguration;
import com.hendisantika.mailgunemailsender.entity.Email;
import com.hendisantika.mailgunemailsender.exception.NoBodyException;
import com.hendisantika.mailgunemailsender.exception.NoRecipientsException;
import com.hendisantika.mailgunemailsender.exception.NoSubjectException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/04/21
 * Time: 12.57
 */
@SpringBootTest(classes = {
        EmailBuilder.class,
})
@EnableConfigurationProperties(EmailSenderConfiguration.class)
public class EmailBuilderTest {
    private final String SUBJECT = "Subject";
    private final String BODY = "Email body";
    private final String DUMMY_ADDRESS = "test@testing.com";
    private final String SENDER_EMAIL = "info@sender.com";
    private final String SENDER_NAME = "Testy Testerson";
    private final String SENDER_FIELD = "Testy Testerson <info@sender.com>";
    @Autowired
    private EmailBuilder emailBuilder;
    @Autowired
    private EmailSenderConfiguration configuration;

    private Set<String> getEmailSet() {
        Set<String> emailSet = new HashSet<>();
        emailSet.add(DUMMY_ADDRESS);

        return emailSet;
    }

    private String buildSenderField(String senderName, String senderEmail) {
        return senderName + " " + "<" + senderEmail + ">";
    }

    @Test
    public void testValidEmail() {
        Email testAgainst = new Email();
        testAgainst.setSubject(SUBJECT);
        testAgainst.setBody(BODY);
        testAgainst.addRecipient(DUMMY_ADDRESS);
        testAgainst.setSender(SENDER_FIELD);

        Email email = emailBuilder
                .builder()
                .subject(SUBJECT)
                .body(BODY)
                .addRecipient(DUMMY_ADDRESS)
                .sender(SENDER_NAME, SENDER_EMAIL)
                .build();

        assertThat(email).isEqualToComparingFieldByFieldRecursively(testAgainst);
    }

    @Test
    public void testDefaultValuesFromPropertiesFile() {
        Email email = emailBuilder
                .builder()
                .useDefaults()
                .body("Hello!")
                .build();

        assertThat(email.getBody()).isNotEmpty();
        assertThat(email.getSubject()).isEqualTo(configuration.getDefaultSubject());
        assertThat(email.getRecipients()).isEqualTo(configuration.getDefaultRecipients());
        assertThat(email.getSender()).isEqualTo(buildSenderField(configuration.getDefaultSenderName(),
                configuration.getDefaultSenderEmailAddress()));
    }

    @Test
    public void testDefaultValuesSetProgramatically() {
        Set<String> defaultRecipients = getEmailSet();
        emailBuilder.setDefaultRecipients(defaultRecipients);
        emailBuilder.setDefaultSubject(SUBJECT);
        emailBuilder.setDefaultSenderEmailAddress(SENDER_EMAIL);
        emailBuilder.setDefaultSenderName(SENDER_NAME);

        Email email = emailBuilder
                .builder()
                .useDefaults()
                .body(BODY)
                .build();

        assertThat(email.getRecipients()).isEqualTo(defaultRecipients);
        assertThat(email.getSubject()).isEqualTo(SUBJECT);
        assertThat(email.getSender()).isEqualTo(SENDER_FIELD);
    }

    @Test
    public void testDefaultValuesUsingUseDefaultMethods() {
        Email email = emailBuilder
                .builder()
                .useDefaultRecipients()
                .useDefaultSender()
                .useDefaultSubject()
                .body(BODY)
                .build();

        assertThat(email.getRecipients()).isEqualTo(configuration.getDefaultRecipients());
        assertThat(email.getSender()).isEqualTo(buildSenderField(configuration.getDefaultSenderName(),
                configuration.getDefaultSenderEmailAddress()));
        assertThat(email.getSubject()).isEqualTo(configuration.getDefaultSubject());
    }

    @Test
    public void testNoSubjectThrowsNoSubjectException() {
        Assertions.assertThrows(NoSubjectException.class, () -> {
            Email email = emailBuilder
                    .builder()
                    .body(BODY)
                    .addRecipient(DUMMY_ADDRESS)
                    .sender(SENDER_NAME, SENDER_EMAIL)
                    .build();

        });
    }

    @Test
    public void testNoBodyThrowsNoBodyException() {
        Assertions.assertThrows(NoBodyException.class, () -> {
            Email email = emailBuilder
                    .builder()
                    .subject(SUBJECT)
                    .addRecipient(DUMMY_ADDRESS)
                    .sender(SENDER_NAME, SENDER_EMAIL)
                    .build();
        });
    }

    @Test
    public void testNoRecipientsThrowsNoRecipientsException() {
        Assertions.assertThrows(NoRecipientsException.class, () -> {
            Email email = emailBuilder
                    .builder()
                    .subject(SUBJECT)
                    .body(BODY)
                    .sender(SENDER_NAME, SENDER_EMAIL)
                    .build();
        });
    }
}
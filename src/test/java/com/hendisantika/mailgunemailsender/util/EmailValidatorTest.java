package com.hendisantika.mailgunemailsender.util;

import com.hendisantika.mailgunemailsender.exception.InvalidEmailException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/04/21
 * Time: 14.36
 */
@SpringBootTest(classes = {
        EmailValidator.class
})
class EmailValidatorTest {
    private final String VALID_EMAIL = "test@test.com";
    private final String INVALID_EMAIL = "malformed@bad_example.123";

    @Autowired
    private EmailValidator validator;

    @Test
    public void testValidatesValidEmail() {
        validator.validate(VALID_EMAIL);
    }

    @Test
    public void testThrowsOnInvalidEmail() {
        Assertions.assertThrows(InvalidEmailException.class, () -> {
            validator.validate(INVALID_EMAIL);
        });
    }
}
package com.hendisantika.mailgunemailsender.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.48
 */
@Data
@Component
@ComponentScan(basePackages = "com.hendisantika.mailgunemailsender")
@ConfigurationProperties(prefix = "mailgun.email-sender")
public class EmailSenderConfiguration {
    private String messagesUrl;
    private String domain;
    private String apiKey;
    private String defaultSubject;
    private String defaultSenderEmailAddress;
    private String defaultSenderName;
    private Set<String> defaultRecipients;
}

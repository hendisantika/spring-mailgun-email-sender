package com.hendisantika.mailgunemailsender.exception;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.43
 */
public class InvalidEmailException extends EmailException {
    public InvalidEmailException(String email) {
        super("Invalid Email: " + email);
    }

    public InvalidEmailException(List<String> emails) {
        this(emails.stream().map(e -> e + "\n").collect(Collectors.joining()));
    }
}
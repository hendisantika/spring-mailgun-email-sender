package com.hendisantika.mailgunemailsender.exception;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.46
 */
@Data
public class MailgunResponse {
    private String id;
    private String message;
}

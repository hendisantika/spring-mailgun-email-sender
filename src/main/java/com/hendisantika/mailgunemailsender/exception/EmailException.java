package com.hendisantika.mailgunemailsender.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.41
 */
public class EmailException extends RuntimeException {
    public EmailException(String s) {
        super(s);
    }
}
package com.hendisantika.mailgunemailsender.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.43
 */
public class NoBodyException extends EmailException {
    public NoBodyException() {
        super("No Email body provided!");
    }
}
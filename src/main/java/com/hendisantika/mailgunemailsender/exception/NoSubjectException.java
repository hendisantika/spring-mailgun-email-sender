package com.hendisantika.mailgunemailsender.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/04/21
 * Time: 09.45
 */
public class NoSubjectException extends EmailException {
    public NoSubjectException() {
        super("No Email subject provided!");
    }
}
package com.hendisantika.mailgunemailsender.entity;

import lombok.Data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/04/21
 * Time: 09.35
 */
@Data
public class Email {
    private Set<String> recipients;
    private String sender;
    private String subject;
    private String body;

    public void addRecipient(String recipient) {
        if (recipients == null) {
            recipients = new HashSet<>();
        }

        this.recipients.add(recipient);
    }

    @Override
    public String toString() {
        StringBuilder formattedRecipients = new StringBuilder().append("[\n");
        recipients.forEach(r -> formattedRecipients.append("\t").append(r).append("\n"));
        formattedRecipients.append("]\n");

        StringBuilder formattedBody = new StringBuilder();
        Arrays.stream(body.split("\n")).forEach(line -> formattedBody.append("\t").append(line).append("\n"));

        String sb = "----------------------------------------------------------\n" +
                "Recipients: " + formattedRecipients +
                "Sender    : [" + sender + "]\n" +
                "Subject   : " + subject + "\n" +
                "Body      :\n" + formattedBody +
                "----------------------------------------------------------\n";

        return sb;
    }
}

package com.hendisantika.mailgunemailsender.service;

import com.hendisantika.mailgunemailsender.config.EmailSenderConfiguration;
import com.hendisantika.mailgunemailsender.entity.Email;
import com.hendisantika.mailgunemailsender.exception.EmailException;
import com.hendisantika.mailgunemailsender.exception.InvalidEmailException;
import com.hendisantika.mailgunemailsender.exception.MailgunResponse;
import com.hendisantika.mailgunemailsender.util.EmailValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/04/21
 * Time: 12.48
 */
@Service
@Slf4j
public class EmailSender {
    private final String apiKey;
    private final String messagesUrl;
    private final EmailValidator emailValidator;
    private final WebClient webClient;

    @Autowired
    public EmailSender(EmailSenderConfiguration configuration, EmailValidator emailValidator) {
        this.apiKey = configuration.getApiKey();
        this.messagesUrl = configuration.getMessagesUrl();
        this.emailValidator = emailValidator;
        this.webClient = WebClient.builder().baseUrl(messagesUrl).build();
    }

    private Set<String> getValidEmailRecipients(Email email) {
        Set<String> recipients = new HashSet<>();
        for (String recipient : email.getRecipients()) {
            try {
                emailValidator.validate(recipient);
                recipients.add(recipient);
            } catch (InvalidEmailException e) {
                log.error(e.getMessage());
            }
        }

        return recipients;
    }

    public MailgunResponse sendEmailToApi(String from, Set<String> recipients, String subject, String body) {
        log.debug("Sending Email: " +
                "From: " + from + "\n" +
                "Recipient(s): " + recipients.toString() + "\n" +
                "Subject: " + subject + "\n");

        MailgunResponse response = null;
        try {
            response = webClient.post()
                    .uri(uriBuilder -> {
                        recipients.forEach(r -> uriBuilder.queryParam("to", r));
                        uriBuilder.queryParam("from", from);
                        uriBuilder.queryParam("subject", subject);
                        uriBuilder.queryParam("text", body);

                        return uriBuilder.build();
                    })
                    .headers(h -> h.setBasicAuth("api", apiKey))
                    .retrieve()
                    .bodyToMono(MailgunResponse.class)
                    .block();
        } catch (WebClientException e) {
            e.printStackTrace();
        }

        return response;
    }

    public MailgunResponse sendEmail(Email email) throws EmailException {
        Set<String> recipients = getValidEmailRecipients(email);
        if (recipients.isEmpty()) {
            return null;
        }

        return sendEmailToApi(email.getSender(), recipients, email.getSubject(), email.getBody());
    }

    public List<MailgunResponse> sendEmails(List<Email> emails) {
        return emails
                .stream()
                .map(this::sendEmail)
                .collect(Collectors.toList());
    }
}

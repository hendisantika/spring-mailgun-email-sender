package com.hendisantika.mailgunemailsender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailgunEmailSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailgunEmailSenderApplication.class, args);
    }

}

package com.hendisantika.mailgunemailsender.controller;

import com.hendisantika.mailgunemailsender.config.EmailSenderConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mailgun-email-sender
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/2/22
 * Time: 07:38
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class EmailController {

    private final EmailSenderConfiguration emailSenderConfiguration;

    @GetMapping("/sendEmail")
    public ResponseEntity<String> sendEmail() {
        // create mail sender
        Configuration configuration = new Configuration()
                .domain(this.emailSenderConfiguration.getDomain())
                .apiKey(this.emailSenderConfiguration.getApiKey())
                .from("Aktivasi anaonline", "no-reply@anaonline.id");

        Mail.using(configuration)
                .to("naruto@yopmail.com")
                .subject("API KEY - This is the subject")
                .text("Hello world!\nSend Email using API KEY")
                .build()
                .send();
        log.info("Email already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }

    @GetMapping("/sendEmail2")
    public ResponseEntity<String> sendEmail2() {
        // create mail sender
        Configuration configuration = new Configuration()
                .domain(this.emailSenderConfiguration.getDomain())
                .apiKey(this.emailSenderConfiguration.getApiKey())
                .from("Aktivasi anaonline", "no-reply@anaonline.id");

        Mail.using(configuration)
                .to("naruto@yopmail.com")
                .subject("API KEY - This is the subject")
//                .template("account_activation")
                .template("testing")
                .parameter("v:fullName", "Uzumaki Naruto")
                .build()
                .send();

        log.info("Email with Template already sent via API KEY! Please check your inbox for order confirmation!");
        return new ResponseEntity<>("Please check your inbox!", HttpStatus.OK);
    }
}

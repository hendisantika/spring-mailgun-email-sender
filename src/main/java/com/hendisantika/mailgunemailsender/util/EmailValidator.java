package com.hendisantika.mailgunemailsender.util;

import com.hendisantika.mailgunemailsender.exception.InvalidEmailException;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * Project : mailgun-email-sender
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/04/21
 * Time: 12.45
 */
@Component
public class EmailValidator {
    private final Pattern emailPattern = Pattern.compile("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$");

    public void validate(String email) {
        if (!emailPattern.matcher(email).matches()) {
            throw new InvalidEmailException(email);
        }
    }
}
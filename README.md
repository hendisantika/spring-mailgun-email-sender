# spring-boot-mailgun-sample

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-mailgun-email-sender.git`
2. Navigate to the folder: `cd spring-mailgun-email-sender`
3. Create [MailGun](https://mailgun.com/) Account and set SMTP & API KEY
4. Change SMTP & API KEY with your MailGun credentials
5. Run the application: `mvn clean spring-boot:run`
6. Open your favorite browser: http://localhost:8080/sendEmail - Send basic email using SMTP
7. Open your favorite browser: http://localhost:8080/sendEmail2 - Send basic email using MailGun Template

### Image Screenshot

Send Basic EMail

![Send Basic EMail](img/basic.png "Send Basic EMail")

Send Email Template

![Send Email Template](img/email.png "Send Email Template")